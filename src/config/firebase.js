import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyC3ielHYFAB9dwniLMr-NmuYcxYmmqL7xU",
  authDomain: "chousei-firebase-ddf57.firebaseapp.com",
  databaseURL: "https://chousei-firebase-ddf57.firebaseio.com",
  projectId: "chousei-firebase-ddf57",
  storageBucket: "",
  messagingSenderId: "750014964524",
  appId: "1:750014964524:web:285bab701fa0bb9f"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
